﻿#include "P4.h"
#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


#define COL 7
#define ROW 6
#define CHIPS 21

const unsigned char p1 = 'x';
const unsigned char p2 = 'o';

unsigned char table[ROW + 2][COL + 1] = {
    {' ', '1', '2', '3', '4', '5', '6', '7'},
    {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
    {'1', '.', '.', '.', '.', '.', '.', '.'},
    {'2', '.', '.', '.', '.', '.', '.', '.'},
    {'3', '.', '.', '.', '.', '.', '.', '.'},
    {'4', '.', '.', '.', '.', '.', '.', '.'},
    {'5', '.', '.', '.', '.', '.', '.', '.'},
    {'6', '.', '.', '.', '.', '.', '.', '.'},
};

static void print_table(void) {
  clear_screen();
  for (int i = 0; i < ROW + 2; i++) {
    for (int j = 0; j < COL + 1; j++) {
      printf_s("%c", table[i][j]);
    }
    printf_s("\n");
  }
}

static unsigned short check_input(unsigned int row, unsigned int col) {
  if ((row == 0) || (col == 0)) {
    return 1;
  }
  if ((row + 1 > ROW) || (col > COL)) {
    return 2;
  }
  if (table[row + 1][col] != '.') {
    return 3;
  }
  if (table[row + 1][col] == p1 || table[row + 1][col] == p2) {
    return 3;
  }
  return 0;
}

static unsigned short vertical_check(unsigned int row, unsigned int col,
                                     unsigned int player) {}

static unsigned short horizontal_check(unsigned int row, unsigned int col,
                                       unsigned int player) {}

static unsigned short diagonal_check(unsigned int row, unsigned int col,
                                     unsigned int player) {}

unsigned short check(unsigned int row, unsigned int col, unsigned int player) {
  ;
}

static void modify_table(unsigned int row, unsigned int col,
                         unsigned int player) {
  unsigned char icon;
  if (player == 1) {
    icon = p1;
  } else {
    icon = p2;
  }
  table[row + 1][col] = icon;
  /* unsigned short rc = check(row, col, player);
  return rc; */
}

unsigned short pos(unsigned short player) {
  unsigned int row = inputp('L');
  unsigned int col = inputp('C');
  unsigned short valid = check_input(row, col);
  if (valid != 0) {
    printf_s("%s \n", "Invalid position");
    switch (valid) {
    case 1:
      printf_s("%s \n", "Position can't be 0");
      wait_for_enter();
      break;
    case 2:
      printf_s("%s \n", "Out of bounds");
      wait_for_enter();
      break;
    case 3:
      printf_s("%s \n", "Position not empty");
      wait_for_enter();
      break;
    default:
      break;
    }
  }
  modify_table(row, col, player);
  return 0;
}

int main(void) {
  for (int i = 1; i < CHIPS; i++) {
    unsigned short player;
    unsigned short rc = 127;
    if (i % 2 + 1 == 1) {
      player = 2;
    } else {
      player = 1;
    }
    while (rc != 0) {
      rc = pos(player);
    }
    print_table();
  }
  return 0;
}
