﻿#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void clear_screen(void) {
// Checks for Windows
#ifdef _WIN32
  // ASCII Escape codes for screen clearing on Windows
  puts("\033[2J\033[1; 1H");
  printf("\x1b[%d;%df", 1, 1);
#else
  // ASCII Escape codes for screen clearing on POSIX
  puts("\e[1;1H\e[2J");
#endif
}

void delay(int number_of_seconds) {
  // Converting time into milli_seconds
  int milli_seconds = 1000 * number_of_seconds;

  // Storing start time
  clock_t start_time = clock();

  // looping till required time is not achieved
  while (clock() < start_time + milli_seconds)
    ;
}

int inputp(unsigned char message) {
  int in;
  fflush(stdin);
  printf_s("%c :", message);
  scanf_s("%i", &in);
  fflush(stdin);
  return in;
}

void wait_for_enter(void) {
  printf_s("%s", "Press ENTER to continue");
  getchar();
}
