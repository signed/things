#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define PERCENT 0.05
#define PERCENT2 0.08
#define FLOOR 100
#define FLOOR2 300


void twenty_two(void)
{
	int n;
	scanf_s("%d", &n);
	int cpt = 1;
	double result = 1;
	while (cpt <= n)
	{
		cpt++;
		result = result + cpt;
		printf("1: %lf \n", result);
		printf("1-2: %lf \n", result*result);
	}
	cpt = 1;
	result = 0;
	while (cpt <= n)
	{
		cpt++;
		if ((cpt % 2 != 0) && (cpt + 1 <= n))
		{
			cpt++;
		}
		else if (cpt + 1 > n)
		{
			break;
		}
		result = result + pow(cpt, 2);
		printf("2: %lf \n", result);
	}
	cpt = 1;
	long result_fact = 1;
	while (cpt <= n)
	{
		cpt++;
		result_fact = result_fact * cpt;
		printf("3: %ld \n", result_fact);
	}
}


void price(void)
{
	float n;
	scanf_s("%f", &n);
	if (n >= FLOOR)
	{
		float percent = 1.0 - PERCENT;
		if (n >= FLOOR2)
		{
			percent = 1.0 - PERCENT2;
		}
		float final_price = n * percent;
		float cut = n - final_price;
		printf("Final prince : %g\n", final_price);
		printf("Price cut : %g", cut);
	}
	else
	{
		printf("Not eligible");
	}
}

void main(void)
{
	price();
}